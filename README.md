# Struktura
- DataPrep.ipynb - obsahuje přípravu a prozkoumání dat HM Fashion
- Elsa.ipynb - obsahuje implementaci ELSA
- HM_Elsa.ipynb - obsahuje zpracování dat a naučení modelu
- Graphs.ipynb - obsahuje vytvořené grafy


# mvi-sp Milestone

## Zadání
- Vytvořit rekomendační systém pomocí ELSA (scalablE Linear Shallow Autoencoder) ve frameworku PyTorch.
- Použít rekomendační systém na datasetu H&M Personalized Fashion Recommendations z proběhlé Kaggle soutěže (https://www.kaggle.com/competitions/h-and-m-personalized-fashion-recommendations)
- Prozkoumat možnosti reprezentace latentního podprostoru.

## Stav
Mám dataset nákupů v H&M, který reprezentuje implicitní hodnocení produktů. Vytvořil jsem si sparse matrix kde hodnota 1 značí, že si zákazník koupil zboží. 
Dále jsem rozpracoval model ELSA. Zatím bojuji s pochopením modelu, co má být v které z matic.
![alt ELSA](Elsa.png "ELSA")

## Přečtená literatura:
- Scalable Linear Shallow Autoencoder for Collaborative Filtering https://doi.org/10.1145/3523227.3551482
- Making Linear Autoencoders Work for Large Scale Recommendation Systems https://www.recombee.com/blog/making-linear-autoencoders-work-for-large-scale-recommendation-systems.html
- EASE https://arxiv.org/pdf/1905.03375.pdf
